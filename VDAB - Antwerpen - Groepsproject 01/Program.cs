﻿using ExtensionMethods;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Les10_GroepsProject
{
    class Program
    {
        public static string gameDataPath = @"C:\Users\super\source\repos\VDAB - Antwerpen - Groepsproject 01\VDAB - Antwerpen - Groepsproject 01\GameData.txt";
        static void Main(string[] args)
        {
            string userName = "";
            int userAge = 0;

            GetUserInfo(ref userName, ref userAge);

            switch (userName)
            {
                case "admin":
                    StockManager();
                    break;
                default:
                    Store(userAge);
                    break;
            }
            Console.ReadKey();
        }

        static void ReadGameData(Game[] games)
        {
            using (StreamReader sr = new StreamReader(gameDataPath))
            {
                string line;
                int i = 2;
                string[] objects = new string[i]; //Read the amount of lines to determine the size of our array
                int lineCount = 0;
                string[] consoleLine;
                string[] stockLine;

                while ((line = sr.ReadLine()) != null)
                {
                    objects = line.Split(";");          //dit maakt de array string[] objects[het aantal ingelezen splits]

                    consoleLine = objects[1].Split("|");
                    stockLine = objects[2].Split("|");

                    games[lineCount] = new Game(objects[0], new bool[] { Boolean.Parse(consoleLine[0]), Boolean.Parse(consoleLine[1]), Boolean.Parse(consoleLine[2]), Boolean.Parse(consoleLine[3]) }
                        , new int[] { Int32.Parse(stockLine[0]), Int32.Parse(stockLine[1]), Int32.Parse(stockLine[2]), Int32.Parse(stockLine[3]) }, Decimal.Parse(objects[3]),
                        DateTime.Parse(objects[4]), Int32.Parse(objects[5]), objects[6]);

                    lineCount++;
                }
            }
        }

        static void SaveAllGameData(Game[] gamesToSave)
        {
            using (StreamWriter sw = new StreamWriter(gameDataPath))
            {
                foreach (Game game in gamesToSave)
                {
                    sw.WriteLine(game.MyToString(2));
                }
            }
        }
        static void SaveSingleGameData(Game gameToSave)
        {
            using (StreamWriter sw = new StreamWriter(gameDataPath, true))
            {
                sw.WriteLine(gameToSave.MyToString(2));
            }
        }

        static void ShowGameList(Game[] gamesToShow)
        {
            int count = 1;
            Console.OutputEncoding = Encoding.UTF8;

            foreach (Game game in gamesToShow)
            {
                string n = $"{count}.";
                Console.Write($"\n{n.PadRight(5)} {game.MyToString(0)}");
                game.PrintPlatformStock();
                MyExtensions.WriteColor($"{game.MyToString(1)}\n", ConsoleColor.DarkCyan);

                Console.WriteLine("".PadRight(172, '_'));
                count++;
            }
        }

        private static void GetUserInfo(ref string name, ref int age)
        {
            Console.Write("Login: ");
            name = Console.ReadLine();
            if (name != "admin")
            {
                Console.WriteLine("Wat is uw leeftijd?");
                bool validInput = false;
                do
                {
                    validInput = Int32.TryParse(Console.ReadLine(), out age);

                    if (age < 0)
                    {
                        MyExtensions.WriteColor("[Geef een geldige leeftijd op:]\n", ConsoleColor.Red);
                        validInput = false;
                    }

                } while (!validInput);
            }
        }

        private static int GetMenuChoice(int menuLength)
        {
            MyExtensions.WriteColor("[Maak uw keuze:]", ConsoleColor.DarkYellow);
            bool validInput = false;
            int menuChoice;
            do
            {
                validInput = Int32.TryParse(Console.ReadLine(), out menuChoice);

                if (menuChoice > menuLength)
                {
                    MyExtensions.WriteColor("[Ongeldige menu keuze, gelieve een andere te kiezen:]\n", ConsoleColor.Red);
                    validInput = false;
                }

            } while (!validInput);

            return menuChoice;
        }

        private static int ValidateIntInput()
        {
            bool validInput = false;
            int i;
            do
            {
                validInput = Int32.TryParse(Console.ReadLine(), out i);

                if (!validInput)
                {
                    MyExtensions.WriteColor("[Ongeldige invoer, gelieve een geheel getal in te voeren:]\n", ConsoleColor.Red);
                }

            } while (!validInput);

            return i;
        }

        private static decimal ValidateDecimalInput()
        {
            bool validInput = false;
            decimal d;
            do
            {
                validInput = Decimal.TryParse(Console.ReadLine(), out d);

                if (!validInput || d < 0)
                {
                    MyExtensions.WriteColor("[Ongeldige invoer, gelieve een positief getal in te voeren:]\n", ConsoleColor.Red);
                }

            } while (!validInput);

            return d;
        }

        private static DateTime ValidateDateInput()
        {
            bool validInput = false;
            DateTime date;
            do
            {
                validInput = DateTime.TryParseExact(Console.ReadLine(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);

                if (!validInput)
                {
                    MyExtensions.WriteColor("[Ongeldige invoer, gelieve het formaat dd/MM/yyyy te gebruiken.]\n", ConsoleColor.Red);
                }

            } while (!validInput);

            return date;
        }

        private static void StockManager()
        {
            Console.Clear();
            Console.WriteLine("1. Game toevoegen.");
            Console.WriteLine("2. Game verwijderen.");
            Console.WriteLine("3. Game aanpassen.");
            Console.WriteLine("4. Afsluiten.");

            int userChoice = GetMenuChoice(4);

            switch (userChoice)
            {
                case 1:
                    AddGame();
                    break;
                case 2:
                    RemoveGameMenu(false);
                    break;
                case 3:
                    AdjustGame();
                    break;
                default:
                    break;
            }
        }

        private static void Store(int age)
        {
            Console.Clear();                                                                //eventueel nog veranderen
            Game.PrintHeaders();
            Game[] gamesToShow = (GetGamesToShow(age));
            ShowGameList(gamesToShow);

            Console.WriteLine("Welk game wil je kopen?");
            int gameToBuy = GetMenuChoice(gamesToShow.Length) - 1;
            MyExtensions.WriteColor($"U hebt gekozen voor: [{gamesToShow[gameToBuy].Name}]\n", ConsoleColor.DarkYellow);
            int count = 0;
            int buyChoice = 0;
            //if (gamesToShow[gameToBuy].Platform.Length > 1)       //dit is altijd 4, als je wil skippen als er maar 1 platform beschikbaar is, aantal true's tellen in gamesToShow[gameToBuy].Platform
            //{
            Console.WriteLine("Voor welk platform wil je dit game kopen?");

            foreach (bool platform in gamesToShow[gameToBuy].Platform)
            {
                if (platform)
                {
                    switch (count)
                    {
                        case 0:
                            //if (games[gameToEdit].Platform[0] == false)
                            Console.WriteLine($"{count + 1}. PC");
                            break;
                        case 1:
                            //if (games[gameToEdit].Platform[1] == false)
                            Console.WriteLine($"{count + 1}. Playstation");
                            break;
                        case 2:
                            //if (games[gameToEdit].Platform[2] == false)
                            Console.WriteLine($"{count + 1}. Xbox");
                            break;
                        case 3:
                            //if (games[gameToEdit].Platform[3] == false)
                            Console.WriteLine($"{count + 1}. Nintendo");
                            break;
                        default:
                            break;
                    }
                }
                count++;
            }
            buyChoice = GetMenuChoice(count);
            //}
            string buyOnPlatform = "";
            switch (buyChoice)
            {
                case 1:
                    buyOnPlatform = "PC";
                    break;
                case 2:
                    buyOnPlatform = "Playstation";
                    break;
                case 3:
                    buyOnPlatform = "Xbox";
                    break;
                case 4:
                    buyOnPlatform = "Nintendo";
                    break;
                default:
                    break;
            }

            MyExtensions.WriteColor($"Wilt u [{gamesToShow[gameToBuy].Name}] voor [{buyOnPlatform}] nu kopen voor [€ {gamesToShow[gameToBuy].SalePrice}]?\n", ConsoleColor.DarkGreen);
            Console.WriteLine("1. Ja");
            Console.WriteLine("2. Nee, ga terug de store");

            int menuChoice = GetMenuChoice(2);

            if (menuChoice == 1)
            {
                MyExtensions.WriteColor($"U hebt net [{gamesToShow[gameToBuy].Name}] voor [{buyOnPlatform}] gekocht! Veel speelplezier!\n", ConsoleColor.DarkGreen);
                Console.WriteLine("Nog een game kopen?");
                Console.WriteLine("1. Ja");
                Console.WriteLine("2. Nee, mijn geld is op");
                menuChoice = GetMenuChoice(2);
                if (menuChoice == 1)
                {
                    Store(age);
                }
                else
                {
                    Console.WriteLine("Tot de volgende keer!");
                }
            }
            else
                Store(age);
        }

        private static Game[] GetGamesToShow(int age)
        {
            Game[] games = new Game[File.ReadLines(gameDataPath).Count()];//Read the amount of lines to determine the size of our array

            ReadGameData(games);

            int teller = 0;

            Game[] gamesToShow = new Game[games.Length];

            foreach (Game game in games)
            {
                if (game.AgeRating <= age)
                {
                    gamesToShow[teller] = game;
                    teller++;
                }
            }

            Array.Resize(ref gamesToShow, teller);

            return gamesToShow;
        }

        private static void AddGame()
        {
            Console.WriteLine("Naam van de game:");
            string gameName = Console.ReadLine();   //name must be EXACT
            int menuChoice;

            bool gameAlreadyExist = CheckIfGameExists(gameName);
            if (gameAlreadyExist)
            {
                Console.WriteLine("Dit spel staat al in de database.");
                Console.WriteLine("Wil je het aanpassen?");
                Console.WriteLine("1. Ja");
                Console.WriteLine("2. Nee, ga terug naar keuze menu.");

                menuChoice = GetMenuChoice(2);

                if (menuChoice == 1)
                {
                    AdjustGame(gameName);
                    return;
                }
                else
                {
                    StockManager();
                    return;
                }
            }
            Game newGame = new Game();
            newGame.Name = gameName;
            newGame.Platform = new bool[4]; //need to initialize the array or we can't add values to it. The 4 should be a variable incase ever more consoles get added.

            for (int i = 0; i < 4; i++) //4 should be made variable or it won't work for more platforms
            {
                Console.WriteLine($"Is het spel speelbaar op {Game.GetSpecificPlatform(i)}?");
                Console.WriteLine("1. Ja");
                Console.WriteLine("2. Nee");

                menuChoice = GetMenuChoice(2);
                newGame.Platform[i] = (menuChoice == 1) ? true : false;
            }

            newGame.InStock = new int[4];
            int stock;

            for (int i = 0; i < 4; i++) //4 should be made variable or it won't work for more platforms
            {
                if (newGame.Platform[i])
                {
                    Console.WriteLine($"Wat is de stock voor de {Game.GetSpecificPlatform(i)} versie?");
                    stock = ValidateIntInput();

                    newGame.InStock[i] = stock;
                }
                else
                    newGame.InStock[i] = 0;
            }

            Console.WriteLine("Wat is de verkoopprijs van het spel?");
            newGame.SalePrice = ValidateDecimalInput();

            Console.WriteLine("Wat is de release date van het spel? dd/mm/yyyy");
            newGame.ReleaseDate = ValidateDateInput();

            Console.WriteLine("Wat is de age rating van het spel?");
            Console.WriteLine("1. 3\n2. 7\n3. 12\n4. 16\n5. 18");
            menuChoice = GetMenuChoice(5);

            switch (menuChoice)
            {
                case 1:
                    newGame.AgeRating = 3;
                    break;
                case 2:
                    newGame.AgeRating = 7;
                    break;
                case 3:
                    newGame.AgeRating = 12;
                    break;
                case 4:
                    newGame.AgeRating = 16;
                    break;
                case 5:
                    newGame.AgeRating = 18;
                    break;
            }


            Console.WriteLine("Welke genre's heeft het spel? (geef alle genres in gescheiden door een ,)");
            newGame.Genre = Console.ReadLine();

            try
            {
                SaveSingleGameData(newGame);
            }
            catch (Exception)
            {
                MyExtensions.WriteColor($"[Er ging iets fout bij het opslaan van de game]\n", ConsoleColor.Red);
            }

            MyExtensions.WriteColor($"[De game {newGame.Name} is succesvol toegevoegd!]\n", ConsoleColor.Green);

            Console.WriteLine("Wilt u nog een spel toevoegen?");
            Console.WriteLine("1. Ja");
            Console.WriteLine("2. Nee, ga terug keuze menu");

            menuChoice = GetMenuChoice(2);

            if (menuChoice == 1)
                AddGame();
            else
                StockManager();

        }

        private static void RemoveGameMenu(bool skip, Game[] games = null)
        {
            if (!skip)
            {
                Console.Clear();
                games = new Game[File.ReadLines(gameDataPath).Count()];
                ReadGameData(games);

                Game.PrintHeaders();
                ShowGameList(games);
            }

            Console.WriteLine($"Welke game wil je verwijderen? 1 - {games.Length}");
            int gameIndex = GetMenuChoice(games.Length) - 1;

            MyExtensions.WriteColor($"Bent u zeker dat u game [{gameIndex + 1}] met als naam [{games[gameIndex].Name}] wilt verwijderen?\n", ConsoleColor.Yellow);

            Console.WriteLine("1. Ja");
            Console.WriteLine("2. Nee");

            int confirmChoice = GetMenuChoice(2);

            if (confirmChoice == 1)
            {
                MyExtensions.WriteColor($"De game met als naam [{games[gameIndex].Name}] is verwijdert.\n", ConsoleColor.Yellow);
                RemoveGame(games, gameIndex); //Will set the object of given index to null
                games = games.Where(c => c != null).ToArray(); //Uses LINQ to make a new array without the null objects https://stackoverflow.com/questions/28193564/c-sharp-remove-null-values-from-object-array
                SaveAllGameData(games); //Write our updated array to the text file
                Console.Clear();
                ShowGameList(games);


                Console.WriteLine($"Wilt u nog een game verwijderen? 1 - {games.Length}");
                Console.WriteLine("1. Ja");
                Console.WriteLine("2. Nee");
                confirmChoice = GetMenuChoice(2);

                if (confirmChoice == 1)
                {
                    RemoveGameMenu(true, games);
                }
                else
                {
                    StockManager();
                }
            }
            else
            {
                RemoveGameMenu(true);
            }
        }

        private static void RemoveGame(Game[] games, int gameIndex)
        {
            games[gameIndex] = null;
        }

        private static void AdjustGame(string gameName = "")
        {
            Game[] games = new Game[File.ReadLines(gameDataPath).Count()];
            ReadGameData(games);
            int gameToEdit = -1;
            if (gameName == "")
            {
                Console.Clear();
                Game.PrintHeaders();
                ShowGameList(games);

                Console.WriteLine("Welk game wil je aanpassen?");
                gameToEdit = GetMenuChoice(games.Length) - 1;
            }
            int gameCounter = 0;
            foreach (Game game in games)
            {
                if (game.Name == gameName)
                {
                    gameToEdit = gameCounter;
                }
                gameCounter++;
            }

            //for (int i = 0; i < 7; i++)
            //{
            //    Console.WriteLine($"{i + 1}. ");
            //}
            Console.WriteLine("Welk veld van de game wil je aanpassen?");

            Console.WriteLine("1. Naam");
            Console.WriteLine("2. Platform");
            Console.WriteLine("3. Stock");
            Console.WriteLine("4. Prijs");
            Console.WriteLine("5. Release date");
            Console.WriteLine("6. Leeftijdscategorie");
            Console.WriteLine("7. Genre");
            Console.WriteLine("8. Niets");

            int categoryToEdit = GetMenuChoice(8);

            switch (categoryToEdit)
            {
                case 1:
                    Console.WriteLine("Nieuwe naam?");
                    games[gameToEdit].Name = Console.ReadLine();
                    break;
                case 2:
                    Console.WriteLine("Wil je een platform toevoegen of verwijderen?");
                    Console.WriteLine("1. Toevoegen");
                    Console.WriteLine("2. Verwijderen");
                    int editPlatform = GetMenuChoice(2);
                    switch (editPlatform)
                    {
                        case 1:
                            Console.WriteLine("Welk platform wil je toevoegen?");
                            int count = 0;
                            foreach (bool platform in games[gameToEdit].Platform)
                            {
                                if (!platform)
                                {
                                    switch (count)
                                    {
                                        case 0:
                                            //if (games[gameToEdit].Platform[0] == false)
                                            Console.WriteLine($"{count + 1}. PC");
                                            break;
                                        case 1:
                                            //if (games[gameToEdit].Platform[1] == false)
                                            Console.WriteLine($"{count + 1}. Playstation");
                                            break;
                                        case 2:
                                            //if (games[gameToEdit].Platform[2] == false)
                                            Console.WriteLine($"{count + 1}. Xbox");
                                            break;
                                        case 3:
                                            //if (games[gameToEdit].Platform[3] == false)
                                            Console.WriteLine($"{count + 1}. Nintendo");
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                count++;
                            }


                            //Console.WriteLine("1. PC");
                            //Console.WriteLine("2. Playstation");
                            //Console.WriteLine("3. Xbox");
                            //Console.WriteLine("4. Nintendo");

                            int addPlatform = GetMenuChoice(count);


                            if (games[gameToEdit].Platform[addPlatform - 1] == true)
                            {
                                Console.WriteLine("Dit platform was al toegevoegd.");
                            }
                            games[gameToEdit].Platform[addPlatform - 1] = true;

                            ShowGameList(games);




                            break;

                        case 2:
                            Console.WriteLine("Welk platform wil je verwijderen?");
                            Console.WriteLine("1. PC");
                            Console.WriteLine("2. Playstation");
                            Console.WriteLine("3. Xbox");
                            Console.WriteLine("4. Nintendo");

                            int removePlatform = GetMenuChoice(4);

                            if (games[gameToEdit].Platform[removePlatform - 1] == false) Console.WriteLine("Dit platform was niet toegevoegd.");
                            else games[gameToEdit].Platform[removePlatform - 1] = false;
                            break;

                    }
                    break;

                case 3:
                    Console.WriteLine("Stock aanpassen voor welk platform?");
                    Console.WriteLine("1. PC");
                    Console.WriteLine("2. Playstation");
                    Console.WriteLine("3. Xbox");
                    Console.WriteLine("4. Nintendo");
                    int platformStockToEdit = GetMenuChoice(4);
                    Console.WriteLine($"Huidige stock = {games[gameToEdit].InStock[platformStockToEdit - 1]}");
                    Console.Write("Voer nieuwe stock in: ");

                    bool validStockChange = false;
                    do
                    {
                        validStockChange = Int32.TryParse(Console.ReadLine(), out games[gameToEdit].InStock[platformStockToEdit - 1]);
                    } while (!validStockChange);
                    break;
                case 4:
                    Console.WriteLine($"Huidige prijs = {games[gameToEdit].SalePrice}");
                    Console.Write("Voer nieuwe prijs in (gebruik , als komma): ");
                    games[gameToEdit].SalePrice = ValidateDecimalInput();
                    break;
                case 5:
                    Console.WriteLine("Huidige release date: {0}", games[gameToEdit].ReleaseDate.ToString("dd/MM/yyyy"));
                    Console.WriteLine("Aanpassen naar? (dd/mm/yyyy:)");
                    games[gameToEdit].ReleaseDate = ValidateDateInput();

                    //ShowGameList(games);
                    break;
                case 6:
                    Console.WriteLine($"Huidige leeftijdscategorie: {games[gameToEdit].AgeRating}");
                    Console.WriteLine("Voer nieuwe leeftijdscategorie in:");
                    Console.WriteLine("1. 3\n2. 7\n3. 12\n4. 16\n5. 18");
                    int chooseAgeRating = GetMenuChoice(5);

                    switch (chooseAgeRating)
                    {
                        case 1:
                            games[gameToEdit].AgeRating = 3;
                            break;
                        case 2:
                            games[gameToEdit].AgeRating = 7;
                            break;
                        case 3:
                            games[gameToEdit].AgeRating = 12;
                            break;
                        case 4:
                            games[gameToEdit].AgeRating = 16;
                            break;
                        case 5:
                            games[gameToEdit].AgeRating = 18;
                            break;
                    }

                    break;
                case 7:
                    Console.WriteLine($"Huidige genre(s): {games[gameToEdit].Genre}");
                    Console.WriteLine("Nieuwe genre(s)? (geef alle genres in gescheiden door een ,)");
                    games[gameToEdit].Genre = Console.ReadLine();
                    ShowGameList(games);
                    break;
                default:
                    StockManager();
                    break;
            }

            SaveAllGameData(games);
            Console.Clear();
            ShowGameList(games);

            Console.WriteLine("Wilt u nog een spel aanpassen?");
            Console.WriteLine("1. Ja");
            Console.WriteLine("2. Nee, ga terug naar de Store Manager");

            int menuChoice = GetMenuChoice(2);

            if (menuChoice == 1)
                AdjustGame();
            else
                StockManager();
        }

        private static bool CheckIfGameExists(string nameOfGame)
        {
            Game[] games = new Game[File.ReadLines(gameDataPath).Count()];
            ReadGameData(games);
            bool alreadyExists = false;

            foreach (Game game in games)
            {
                if (game.Name == nameOfGame)
                {
                    alreadyExists = true;
                }
            }
            return alreadyExists;
        }
    }
}
